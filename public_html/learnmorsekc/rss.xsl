<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/rss">
    <html>
      <head>
        <title><xsl:value-of select="channel/title" /> Ogg RSS feed</title>
        <style type="text/css">h3 { margin-bottom: 0.2em; }</style>
      </head>
      <body>
        <h1>
          <xsl:element name="a">
            <xsl:attribute name="href">
              <xsl:value-of select="channel/link" />
            </xsl:attribute>
            <xsl:value-of select="channel/title" />
          </xsl:element>
        </h1>
        <p><xsl:value-of select="channel/description" /></p>
        <xsl:for-each select="channel/item">
          <div class="item">
            <h3><xsl:value-of select="title"/></h3>
            <div>Posted: <xsl:value-of select="pubDate"/></div>
            <xsl:element name="a">
              <xsl:attribute name="href">
                <xsl:value-of select="enclosure/@url"/>
              </xsl:attribute>
              Listen (MP3)
            </xsl:element>
          </div>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
