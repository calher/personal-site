# Personal Site

My personal site at <https://bluehome.net/csh/>, to be eventually
migrated from plain HTML to [Haunt][1], a static site generator for
GNU Guile Scheme.

Right now, everything is very simplistic.  I clone this repo and then
symlink it to `~/public_html/`.

    cd ~/Projects/
    git clone https://gitlab.com/calher/personal-site
    ln -s personal-site/public_html/ ~/public_html/
    
Yes, even images and other binary files are still on here.  It is not
the best solution, but it is the current state of things.  I do hope
to change it as soon as possible.

[1]: https://dthompson.us/projects/haunt.html
